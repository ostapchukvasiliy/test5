### Quick start
    cp .env.dev .env
    docker-compose up -d
    docker-compose exec laravel.test bash

    composer install
    ./artisan key:generate
    ./artisan migrate
    ./artisan db:seed --class=DatabaseSeeder
    ./artisan db:seed --class=DemoSeeder

### API
Use header "Accept: application/json;"

```
POST auth/login
{
   "identity": "Email or Phone",
   "Password": "Password"
}
```

```
POST auth/register
{
    "full_name": "Full name", 
    "email": "Email", 
    "phone": "Phone", 
    "password": "Password", 
    "password_confirmation": "Password confirmation", 
}
```

```
GET product

filters: 
  - name
  - price_from
  - price_to
  - quantity_from
  - quantity_to
  - properties[color][] = [red, white, black, blue]
  - properties[weight][] = [some float]
```
