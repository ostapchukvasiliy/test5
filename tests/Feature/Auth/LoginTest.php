<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\Traits\UserTrait;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase,
        UserTrait;

    public function testAuthByEmail()
    {
        $response = $this->postJson('/auth/login', [
            'identity' => $this->createUser()->email,
            'password' => $this->userPassword,
        ]);

        $response->assertStatus(200);

    }

    public function testAuthByPhone()
    {
        $response = $this->postJson('/auth/login', [
            'identity' => $this->createUser()->phone,
            'password' => $this->userPassword,
        ]);

        $response->assertStatus(200);
    }
}
