<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\Traits\UserTrait;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase,
        UserTrait;

    public function testRegistration()
    {
        $response = $this->postJson('/auth/register', $this->getUserRegData());
        $response->assertStatus(200);
    }

    public function testUserAlreadyExists()
    {
        $this->createUser();

        $response = $this->postJson('/auth/register', $this->getUserRegData());
        $response->assertStatus(422);
    }
}
