<?php

namespace Tests\Feature\Auth;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\Traits\UserTrait;
use Tests\TestCase;

class LogoutTest extends TestCase
{
    use RefreshDatabase,
        UserTrait;

    public function testLogout()
    {
        $response = $this->postJson('/auth/logout', [], ['Authorization' => $this->getAuthToken()]);
        $response->assertStatus(200);
    }
}
