<?php

namespace Tests\Feature;

use Database\Seeders\DatabaseSeeder;
use Database\Seeders\DemoSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Feature\Traits\UserTrait;
use Tests\TestCase;
use Throwable;

class ProductListTest extends TestCase
{
    use RefreshDatabase,
        UserTrait;

    public function setUp(): void
    {
        parent::setUp();

        // seed the database
        $this->seed(DatabaseSeeder::class);
        $this->seed(DemoSeeder::class);
    }

    /**
     * @throws Throwable
     */
    public function testList()
    {
        $response = $this->getJson('/product', ['Authorization' => $this->getAuthToken()]);
        $response->assertStatus(200);
    }

    /**
     * @throws Throwable
     */
    public function testListWithFilter()
    {
        $url = '/product?' . http_build_query([
            'properties[color][0]' => 'red',
            'properties[color][1]' => 'white',
            'properties[weight][0]' => '816.91',
        ]);
        $response = $this->getJson($url, ['Authorization' => $this->getAuthToken()]);
        $response->assertStatus(200);
    }
}
