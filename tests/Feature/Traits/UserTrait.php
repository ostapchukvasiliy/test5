<?php

namespace Tests\Feature\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Throwable;

trait UserTrait
{
    private string $userPassword = '%Testing';

    private function createUser(): User
    {
        return User::create([
            'full_name' => 'Testing User',
            'phone' => '+77777777777',
            'email' => 'user@test.test',
            'password' => Hash::make($this->userPassword),
        ]);
    }

    private function getUserRegData(): array
    {
        return [
            'full_name' => 'Testing User',
            'phone' => '+77777777777',
            'email' => 'user@test.test',
            'password' => $this->userPassword,
            'password_confirmation' => $this->userPassword,
        ];
    }

    /**
     * @throws Throwable
     */
    private function getAuthToken(): string
    {
        $response = $this->postJson('/auth/login', [
            'identity' => $this->createUser()->email,
            'password' => $this->userPassword,
        ]);
        $response->assertStatus(200);

        $data = $response->decodeResponseJson()->json();
        return "{$data['token_type']} {$data['token']}";
    }
}
