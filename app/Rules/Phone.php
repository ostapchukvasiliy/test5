<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Phone implements Rule
{
    /**
     * @param string $attribute
     * @param string $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return preg_replace('/\+7\d{10}/', '', $value) === '';
    }

    public function message(): string
    {
        return __('validation.custom.phone');
    }
}
