<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class EmailOrPhone implements Rule
{
    /**
     * @param string $attribute
     * @param string $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return (
            $this->validateEmail($value) ||
            $this->validatePhone($value)
        );
    }

    public function message(): string
    {
        return __('validation.custom.email_or_phone');
    }

    private function validateEmail(?string $email): bool
    {
        return Validator::make(
            ['phone' => $email],
            ['phone' => ['required', new Phone]]
        )->passes();
    }

    private function validatePhone(?string $phone): bool
    {
        return Validator::make(
            ['email' => $phone],
            ['email' => 'required|email']
        )->passes();
    }
}
