<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductProperty
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ProductProperty newModelQuery()
 * @method static Builder|ProductProperty newQuery()
 * @method static Builder|ProductProperty query()
 * @method static Builder|ProductProperty whereCreatedAt($value)
 * @method static Builder|ProductProperty whereId($value)
 * @method static Builder|ProductProperty whereName($value)
 * @method static Builder|ProductProperty whereSlug($value)
 * @method static Builder|ProductProperty whereUpdatedAt($value)
 * @mixin Eloquent
 */
class ProductProperty extends Model
{
    use HasFactory;

    public const SLUG_COLOR = 'color';
    public const SLUG_WEIGHT = 'weight';
    public const COLORS = [
        'red', 'black', 'white', 'blue',
    ];

    public function propertyValues(): HasMany
    {
        return $this->hasMany(ProductPropertyValue::class);
    }
}
