<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductPropertyValue
 *
 * @property int $id
 * @property int $product_id
 * @property int $property_id
 * @property string $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ProductPropertyValue newModelQuery()
 * @method static Builder|ProductPropertyValue newQuery()
 * @method static Builder|ProductPropertyValue query()
 * @method static Builder|ProductPropertyValue whereCreatedAt($value)
 * @method static Builder|ProductPropertyValue whereId($value)
 * @method static Builder|ProductPropertyValue whereProductId($value)
 * @method static Builder|ProductPropertyValue wherePropertyId($value)
 * @method static Builder|ProductPropertyValue whereUpdatedAt($value)
 * @method static Builder|ProductPropertyValue whereValue($value)
 * @mixin Eloquent
 */
class ProductPropertyValue extends Model
{
    use HasFactory;

    public function property(): BelongsTo
    {
        return $this->belongsTo(ProductProperty::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
