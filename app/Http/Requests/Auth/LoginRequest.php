<?php

namespace App\Http\Requests\Auth;

use App\Rules\EmailOrPhone;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'identity' => ['required', new EmailOrPhone()],
            'password' => 'required|string',
        ];
    }
}
