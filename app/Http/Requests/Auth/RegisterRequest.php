<?php

namespace App\Http\Requests\Auth;

use App\Rules\Phone;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Password;

class RegisterRequest extends FormRequest
{
    /**
     * Rules request
     */
    public function rules(): array
    {
        return [
            'phone' => [
                'required',
                new Phone,
                'unique:users,phone'
            ],
            'email' => 'required|email|unique:users,email',
            'full_name' => 'required|max:255',
            'password' => [
                'required',
                Password::default(),
                'confirmed',
            ],
            'password_confirmation' => 'required',
        ];
    }
}
