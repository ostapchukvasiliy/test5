<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductListRequest;
use App\Services\ProductService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ProductController extends Controller
{
    public function list(ProductListRequest $request, ProductService $service): LengthAwarePaginator
    {
        return $service->getList($request->validated());
    }
}

