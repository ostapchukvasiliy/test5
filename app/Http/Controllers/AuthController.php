<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\LogoutRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Throwable;

class AuthController extends Controller
{
    /**
     * @throws ValidationException
     */
    public function login(LoginRequest $request, AuthService $service): JsonResponse
    {
        $data = $service->login(
            $request->post('identity'),
            $request->post('password'),
        );

        return response()->json($data);
    }

    public function logout(LogoutRequest $request, AuthService $service): JsonResponse
    {
        $service->logout($request->user());

        return response()->json([]);
    }

    /**
     * @throws Throwable
     */
    public function register(RegisterRequest $request, AuthService $service): JsonResponse
    {
        return response()->json([
            'user' => $service->register($request)
        ]);
    }
}

