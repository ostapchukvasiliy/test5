<?php

namespace App\Services;

use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Throwable;

class AuthService
{
    /**
     * @throws ValidationException
     */
    public function login(string $identity, string $password): array
    {
        $user = $this->findByIdentity($identity);

        if (!$user || !Hash::check($password, $user->password)) {
            throw ValidationException::withMessages([
                'identity' => [__('auth.failed')],
            ]);
        }

        return [
            'token_type' => 'Bearer',
            'token' => $user->createToken('api')->plainTextToken,
            'user' => new UserResource($user)
        ];
    }

    public function logout(User $user): void
    {
        $user->tokens()->delete();
    }

    /**
     * @throws Throwable
     */
    public function register(RegisterRequest $request): UserResource
    {
        return new UserResource(
            $this->createUserByRequest($request)
        );
    }

    private function findByIdentity(string $identity): ?User
    {
        return User::query()
            ->emailOrPhone($identity)
            ->first();
    }

    /**
     * @throws Throwable
     */
    private function createUserByRequest(RegisterRequest $request): User
    {
        $user = new User;
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->full_name = $request->get('full_name');
        $user->password = Hash::make($request->get('password'));
        $user->saveOrFail();

        return $user;
    }
}
